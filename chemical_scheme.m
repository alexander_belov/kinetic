function [u_hat, t_hat] = chemical_scheme( u, t, step )
% One step of the chemical solver. Included are the chemical schemes with 
% 1 and 2 iterations in "time" and "arc length" arguments.
global T; global u0; global solver;
U0 = max(u0); J = length(u); u_hat = zeros(J,1);
if ( strcmp(solver,'time_1') == 1 )
    [psi,phi] = right_hand(u);
    u_hat = iteration(u, psi, phi, step);
    t_hat = t + step;
elseif ( strcmp(solver,'time_2') == 1 )
    [psi,phi] = right_hand(u);
    u_hat = iteration(u, psi, phi, step);
    u_half = (u + u_hat)/2;
    [psi,phi] = right_hand(u_half);
    u_hat = iteration(u, psi, phi, step);
    t_hat = t + step;
elseif ( strcmp(solver,'arc_length_1') == 1 )
    [psi,phi] = right_hand(u);
    f = (psi - u.*phi).^2;
    S = sqrt(1/T^2 + sum(f)/U0^2);
    psi = psi*T/S; phi = phi*T/S;   
    u_hat = iteration(u, psi, phi, step);
    phi_t = 1/S; psi_t = 0;
    t_hat = iteration( t, phi_t, psi_t, step );
elseif ( strcmp(solver,'arc_length_2') == 1 )
    [psi,phi] = right_hand(u);
    f = (psi - u.*phi).^2;
    S = sqrt(1/T^2 + sum(f)/U0^2);
    psi = psi*T/S; phi = phi*T/S;   
    u_hat = iteration( u, psi, phi, step );
    u_half = (u + u_hat)/2;
    [psi,phi] = right_hand( u_half );
    f = (psi - u.*phi).^2;
    S = sqrt( 1/T^2 + sum(f)/U0^2 );
    psi = psi*T/S; phi = phi*T/S;   
    u_hat = iteration( u, psi, phi, step );
    phi_t = 1/S; psi_t = 0;
    t_hat = iteration( t, phi_t, psi_t, step );     
end
end