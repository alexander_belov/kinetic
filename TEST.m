u0 = 0.5; % initial condition
int_span = 1; % integration interval
epsilon_user = 1e-6; % required accuracy
solver_name = 'arc_length_2'; % solver
[u, epsilon] = Kinetic(u0, int_span, epsilon_user, solver_name);