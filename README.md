MATLAB package for solving problem of chemical kinetics with guaranteed accuracy

The package fits for stand-alone using and as a part of applied programs for gas dynamics with chemical kinetics.

The package is run by the "Kinetic" function (see "TEST.m" for an example). Detailed comments on the work of the functions are given in the m-files.