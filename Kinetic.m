function[u, epsilon] = Kinetic(initial_cond, Time, epsilon_user, solver_name)
% Solves kinetics problem with guaranteed accuracy. The user sets
% initial conditions, integration interval, required relative accuracy
% and the solver name (default: 2nd order scheme in "time" argument).
% After that, the calculation on a sequence of nested is carried out until
% relative accuracy determined by Richardson method is less than 
% user-required accuracy. The adjusting parameter is the step of the 
% most coarse mesh "initial_step".
global u0; global T; global solver;
u0 = initial_cond; T = Time; solver = solver_name; J = length(u0);
initial_step = T/10; % adjusting parameter
max_grid_num = 15; precision = zeros(1,max_grid_num);
if( nargin < 4 ); solver_name = 'time_2'; end
if(strcmp(solver,'time_1') == 1||strcmp(solver,'arc_length_1') == 1)
    p = 1;
elseif(strcmp(solver,'time_2') == 1||strcmp(solver,'arc_length_2') == 1)
    p = 2;
end
condition_accuracy = 1; m = 1;
while ( condition_accuracy )
    if ( m == 1)
        step = initial_step;
        [u,t] = chemical_solver( step );
        u1 = u;  t1 = t;
        dim = size(u); N = dim(2) - 1; N0 = N;
    else
        u1 = u2; t1 = t2;
    end
    m = m+1; N = 2*N; step = step/2;
    [u,t] = chemical_solver( step, N );
    u2 = u; t2 = t;
    precision_u = richardson( u1, u2, p );
    if( strcmp(solver_name,'arc_length_1') == 1 ||...
            strcmp(solver_name,'arc_length_2') == 1 )
        precision_t = richardson( t1, t2, p );
        for j = 1:J
            for n = 1:N/2+1
                [psi,phi] = right_hand( u(:,2*n-1) );
                precision_u(j,n) = precision_u(j,n) - ...
                    precision_t(n)*(psi(j) - u(j,2*n-1)*phi(j));
            end
        end
    end
    ic_sum = sum(u0); precision(m-1) = max(max(abs(precision_u)))/ic_sum;
    condition_mesh_num = m < max_grid_num;
    condition_accuracy = precision(m-1) > epsilon_user;
    if( condition_mesh_num == 0 )
        str_warn = 'Warning! Reached maximim mesh number.';
        str_acc = strcat(' Current accuracy is: epsilon=',...
            mat2str(precision(m-1)) );
        disp( strcat(str_warn,str_acc) );
        choice = input('Do you wish to proceed? 1 -> yes, 0 -> no: ');
        if( choice == 0 ); break; end;      
    end
end
actual_grid_num = m; precision_output = zeros(1,actual_grid_num - 1);
for m = 1:actual_grid_num - 1
    precision_output(m) = precision(m);
end
epsilon = precision_output(actual_grid_num-1);
illustrations(t, u, N0, precision_output);
end