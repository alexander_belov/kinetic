function illustrations( t, u, N0, precision_output )
% Auxiliary graphics: solution obtained on the thickest mesh and
% accuracy curve
dim = size(u); J = dim(1); N = dim(2);
figure; xlabel('t'); ylabel('u'); title('SOLUTION'); hold on;
for j = 1:J
    c = j/5 - floor(j/5);
    plot(t,u(j,:),'Color',[c,c,c],'LineWidth',2);
end
str = 'u(1)';
for j = 1:J-1
    str0 = strcat('u(',mat2str(j+1),')'); str = char(str,str0);
end
if( J<=10 ); legend(str); end
figure; xlabel('lg N'); ylabel('lg epsilon'); title('RELATIVE ACCURACY');
hold on
NN = zeros(1,length(precision_output));
for m = 1:length(precision_output)
    NN(m) = N0*2^(m);
end
plot(log10(NN),log10(precision_output),...
    '-ok','MarkerEdgeColor','k','MarkerFaceColor','k','MarkerSize',11)
end