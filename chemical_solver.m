function [u,t] = chemical_solver( step, N )
% Solves the ODE system with given step. If the number of steps is unknown
% (on the first mesh for example) the calculation is carried out until the
% given time is reached.
global u0; global T;
J = length(u0);
if( nargin < 2 )
    u = zeros(J,1e+6); t = zeros(1,1e+6);
    u(:,1) = u0;
    n = 1;
    while(t(n) < T)
        [u(:,n+1),t(n+1)] = chemical_scheme( u(:,n), t(n), step );
        n = n+1;
    end
    N1 = n;
    u_truncated = zeros(J,N1); t_truncated = zeros(1,N1);  
    for n = 1:N1
        u_truncated(:,n) = u(:,n); t_truncated( n ) = t(n);
    end
    u = u_truncated; t = t_truncated;        
else
    u = zeros(J,N+1); t = zeros(1,N+1);
    u(:,1) = u0;
    for n = 1:N
        [u(:,n+1), t(n+1)] = chemical_scheme( u(:,n), t(n), step );
    end
end
end