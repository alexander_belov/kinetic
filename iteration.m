function u_hat = iteration( u, psi, phi, step )
% Performs one chemical scheme iteration for one mesh step.
J = length(u); u_hat = zeros(J,1);
for j = 1:J
    u_hat(j) = ( u(j) + step*psi(j)*(1 + step*phi(j)/2) )/...
        (1 + step*phi(j) + step^2*phi(j)^2/2 );
end
end